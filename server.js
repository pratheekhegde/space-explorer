var express = require('express')
var entries = require('./mock-db-data/entries')
var assets = require('./mock-db-data/assets')
var spaces = require('./mock-db-data/spaces')
var noResults = require('./mock-db-data/no-results')
var user1 = require('./mock-db-data/user1')
var user2 = require('./mock-db-data/user2')
var cors = require('cors')

var app = express()
app.use(cors())

// For Spaces
app.get('/space', (req, res, next) => {
  res.json(spaces)
})

// For Space Assets
app.get('/space/yadj1kx9rmg0/assets', (req, res, next) => {
  res.json(assets)
})

app.get('/space/yadj1kx9rmg01/assets', (req, res, next) => {
  res.json(noResults)
})

app.get('/space/yadj1kx9rmg02/assets', (req, res, next) => {
  res.json(noResults)
})

// For Space entries
app.get('/space/yadj1kx9rmg0/entries', (req, res, next) => {
  res.json(entries)
})

app.get('/space/yadj1kx9rmg01/entries', (req, res, next) => {
  res.json(noResults)
})

app.get('/space/yadj1kx9rmg02/entries', (req, res, next) => {
  res.json(noResults)
})

// users
app.get('/users/4FLrUHftHW3v2BLi9fzfjU', (req, res, next) => {
  res.json(user1)
})

app.get('/users/4FLrUHftHW3v2BLi9fzfjU2', (req, res, next) => {
  res.json(user2)
})

// 404
app.get('*', function (req, res) {
  res.json({ 'message': 'asset not found' })
})

app.listen(3000, function () {
  console.info('Mock Server running at localhost:3000')
})
