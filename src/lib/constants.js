const SPACE_ENTRIES_FETCH_ERROR = 'Unable to retrive Entries of the Space'
const SPACE_ASSETS_FETCH_ERROR = 'Unable to retrive Assets of the Space'
const ALL_SPACE_FETCH_ERROR = 'Unable to retrive All the Spaces'

export default {
  SPACE_ENTRIES_FETCH_ERROR,
  SPACE_ASSETS_FETCH_ERROR,
  ALL_SPACE_FETCH_ERROR
}
