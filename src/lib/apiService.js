import ky from 'ky' // @sindresorhus is 👌🏻

const API_URL = 'http://localhost:3000'

const getSpaces = async () => ky.get(`${API_URL}/space`).json()

const getSpacesById = async (spaceId) => ky.get(`${API_URL}/space/${spaceId}`).json()

const getSpaceEntries = async (spaceId) => ky.get(`${API_URL}/space/${spaceId}/entries`).json()

const getSpaceAssets = async (spaceId) => ky.get(`${API_URL}/space/${spaceId}/assets`).json()

const getUserById = async (userId) => ky.get(`${API_URL}/users/${userId}`).json()

export default {
  getSpaces,
  getSpacesById,
  getSpaceEntries,
  getSpaceAssets,
  getUserById
}
