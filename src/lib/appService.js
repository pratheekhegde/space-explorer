import { format } from 'date-fns'
import Api from './apiService'
import { store } from '../store'
import CONSTANTS from './constants'

// cache user data to avoid unecessary api calls
let userHashMap = {}

// for setting the user cache
const _setUserCache = (userId, obj) => {
  userHashMap[userId] = Object.assign({}, obj)
}
// for getting the user cache
const _getUserCache = userId => userHashMap[userId]

// for getting the user info from cache/network
let _getUserInfo = async function (userId) {
  if (!_getUserCache(userId)) {
    const userInfo = await Api.getUserById(userId)
    const flattenedUserInfo = { name: userInfo.fields.name, role: userInfo.fields.name, id: userInfo.sys.id }
    _setUserCache(userId, flattenedUserInfo) // update the user cache
    return flattenedUserInfo
  } // else console.info(`Fetched ${userId} from cache`)
  return _getUserCache(userId)
}

// helper for getting user info
const getUserbyId = async userID => _getUserInfo(userID)

// resolve all the references to user as the response data doesn't contained the joined records
const getResolvedSpaceEntries = async (spaceId) => {
  try {
    const entries = await Api.getSpaceEntries(spaceId)
    let resolvedEntites = []
    // using map will put the resolved promises out of order
    for (const entity of entries.items) {
      resolvedEntites.push({
        title: entity.fields.title,
        summary: entity.fields.summary,
        createdBy: await _getUserInfo(entity.sys.createdBy),
        updatedBy: await _getUserInfo(entity.sys.updatedBy),
        lastUpdated: format(entity.sys.updatedAt, 'DD-MM-YYYY')
      })
    }
    return resolvedEntites
  } catch (e) {
    store.commit('setError', { message: CONSTANTS.SPACE_ENTRIES_FETCH_ERROR })
    return []
  }
}

// resolve all the references to user as the response data doesn't contained the joined records
const getResolvedSpaceAssets = async (spaceId) => {
  try {
    const assets = await Api.getSpaceAssets(spaceId)
    let resolvedAssets = []
    // using map will put the resolved promises out of order
    for (const asset of assets.items) {
      resolvedAssets.push({
        title: asset.fields.title,
        contentType: asset.fields.contentType,
        fileName: asset.fields.fileName,
        createdBy: await _getUserInfo(asset.sys.createdBy),
        updatedBy: await _getUserInfo(asset.sys.updatedBy),
        lastUpdated: format(asset.sys.updatedAt, 'DD-MM-YYYY')
      })
    }
    return resolvedAssets
  } catch (e) {
    store.commit('setError', { message: CONSTANTS.SPACE_ASSETS_FETCH_ERROR })
    return []
  }
}

// service for getting all spaces
const getAllSpaces = async () => {
  try {
    const spaces = await Api.getSpaces()
    let processedSpaces = []
    // using map will putsthe resolved promises out of order
    for (const space of spaces.items) {
      processedSpaces.push({
        title: space.fields.title,
        description: space.fields.description,
        spaceId: space.sys.id,
        path: `/spaceexplorer/${space.sys.id}`,
        createdBy: await _getUserInfo(space.sys.createdBy)
      })
    }
    return processedSpaces
  } catch (e) {
    store.commit('setError', { message: CONSTANTS.ALL_SPACE_FETCH_ERROR })
    return []
  }
}
export default {
  getResolvedSpaceEntries,
  getResolvedSpaceAssets,
  getUserbyId,
  getAllSpaces
}
