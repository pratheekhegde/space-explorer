import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export const store = new Vuex.Store({
  state: {
    error: {
      message: null,
      path: null
    }
  },
  mutations: {
    setError (state, payload) {
      state.error = {
        message: payload.message,
        path: window.location.pathname
      }
    }
  },
  getters: {
    errors (state) {
      return state.error
    }
  }
})
