import Vue from 'vue'
import Router from 'vue-router'

// Layouts
import SpaceExplorer from './views/SpaceExplorer'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/spaceexplorer/:spaceId',
      component: SpaceExplorer,
      name: 'spacePage'
    },
    { path: '*', redirect: { name: 'spacePage' } }
  ]
})
