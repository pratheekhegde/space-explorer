import { createLocalVue, shallowMount, mount } from '@vue/test-utils'
import SpaceComponent from '@/components/SpaceComponent.vue'
import Vuetify from 'vuetify'
import Vue from 'vue'
import fetch from 'node-fetch'
import AppService from '../../src/lib/appService'

describe('SpaceComponent.vue', () => {
  let localVue

  beforeEach(() => {
    localVue = createLocalVue()
    Vue.use(Vuetify)
    // setup fetch so that ky works in node
    global.window = {}
    global.window.fetch = fetch
    global.window.Headers = fetch.Headers
  })

  it('it must load entries data on load', async () => {
    // localVue.config.errorHandler = done
    const spaceId = 'yadj1kx9rmg0'
    const wrapper = shallowMount(SpaceComponent, {
      localVue,
      propsData: { spaceId }
    })
    const entriesTobeMatched = await AppService.getResolvedSpaceEntries(spaceId)
    expect(wrapper.vm.spaceEntries).toEqual(entriesTobeMatched)
  })

  it('it must load assets data on clicking assets tab', async () => {
    // localVue.config.errorHandler = done
    const spaceId = 'yadj1kx9rmg0'
    const wrapper = mount(SpaceComponent, {
      localVue,
      propsData: { spaceId }
    })
    // simulate clicking of assets
    wrapper.find('#assets-button a').trigger('click')
    const assetsTobeMatched = await AppService.getResolvedSpaceAssets(spaceId)
    expect(wrapper.vm.spaceAssets).toEqual(assetsTobeMatched)
    expect(wrapper.vm.activeTab).toBe(1)
  })

  it('it must load entries data on clicking entries tab', async () => {
    // localVue.config.errorHandler = done
    const spaceId = 'yadj1kx9rmg0'
    const wrapper = mount(SpaceComponent, {
      localVue,
      propsData: { spaceId }
    })

    // set the tab to assets
    wrapper.setData({ activeTab: 1 })

    // simulate clicking of entries
    wrapper.find('#entries-button a').trigger('click')
    const entriesTobeMatched = await AppService.getResolvedSpaceEntries(spaceId)
    expect(wrapper.vm.spaceEntries).toEqual(entriesTobeMatched)
    expect(wrapper.vm.activeTab).toBe(0)
  })
})
