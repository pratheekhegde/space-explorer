import { mount, createLocalVue } from '@vue/test-utils'
import SpaceExplorer from '@/views/SpaceExplorer.vue'
import Vuex from 'vuex'
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import AppService from '../../src/lib/appService'

describe('SpaceExplorer.vue', () => {
  let store
  let wrapper
  let localVue
  let router
  beforeEach(() => {
    localVue = createLocalVue()
    router = new VueRouter()
    localVue.use(Vuex)
    Vue.use(Vuetify)
    store = new Vuex.Store({
      state: {
        errors: null
      },
      getters: {
        errors (state) {
          return state.error
        }
      }
    })
  })

  it('it should load the title Space Explorer', () => {
    wrapper = mount(SpaceExplorer, { store, localVue, router })
    expect(wrapper.text()).toContain('Space Explorer')
  })

  it('it should load the spaces in the side nav bar', async () => {
    const $route = {
      path: '/spaceexplorer/:spaceId',
      params: {
        spaceId: 'yadj1kx9rmg0'
      }
    }

    wrapper = mount(SpaceExplorer, { store,
      localVue,
      router,
      mocks: {
        $route
      } })

    const spacesTobeMatched = await AppService.getAllSpaces()
    expect(wrapper.vm.sideMenu).toEqual(spacesTobeMatched)
  })
})
