# Atlassian UI Coding Exercise
 
>  Space Explorer By Pratheek Hegde

### Interactions covered
- Users can navigate spaces by clicking on a vertical tab on the left. ✅
- Users can navigate between the entries and assets tab for a given space. ✅
- Users can sort (on the client side) by clicking column headers. (For the purposes of this test, assume the API does not support server-side sort). ✅
- URL Route should contain a Space GUID. ✅
- Visiting the base URL should redirect to the first space returned by the /space end- point. ✅
- When API fails retry 3 times and handle it gracefully in ui. (Thanks to [ky](https://github.com/sindresorhus/ky)) ✅
- Add few unit tests for the Vue Components. ✅

### Steps to Run
- Install `yarn` if you don't have.
    ```
    npm install --global yarn
    ```
- Install all the dependencies 📦.
    ```
    yarn install
    ```

- Run the Mock API server.
    ```
    yarn run mock-api
    ```

- Open another terminal and run the development or production server.
    - Compile and hot-reload UI for development.
        ```
        yarn run serve
        ```
        and navigate to `http://localhost:8080/`
    - Compile and build UI for production.
        ```
        yarn run build
        ```
    - Serve Production UI Bundle locally. 
        ```
        npx serve dist/
        ```

- Open another terminal and run the UI Unit Tests if you want.
    ```
    yarn run test:unit --verbose
    ```
    Make sure the Mock API Server is running while running the unit tests.
### Some Trivia
- The document for this challenge is in `docs/UI_Coding_Exercise_Aug2018.pdf`.
- The Mock server will run on port `3000`.
- If you are planning to change the port of Mock API Server, remember to update the `API_URL` in the `src/lib/apiService.js` file.

## License

MIT
